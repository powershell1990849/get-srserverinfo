<#
.SYNOPSIS
    Skrypt do pobierania informacji z serwer�w.

.DESCRIPTION
    Skrypt pobiera ze wskazanych serwer�w/komputer�w informacje: nazwa, Adres IP, adresy serwer�w DNS, system operacyjny, wolna przestrze� na dyskach, ilo�� pami�ci RAM, typ wirtualny/fizyczny, numer seryjny i error.
    We w�a�ciwo�ci Error przechowywany jest komunikat b��du je�li taki wyst�pi� podczas odpytywania serwera przez skrypt.
    Je�li w katalogu g��wnym skryptu umie�cimy plik services.txt z nazwami us�ug wpisanymi w oddzielnych wierszach skrypt dodatkowo sprawdzi czy na serwerach s� wskazane us�ugi.
    Poniewa� skrypt zwraca objekty mo�na z nimi dalej pracowa� wybieraj�c interesuj�ce nas w�a�ciwo�ci np: ilo�� wolnego miejsca na dysku, lub objekty spe�niaj�ce okre�lone w�a�ciwo�ci np: tylko maszyny wirtualne.

.PARAMETER SearchBase
    �cie�ka w formacje: 'OU=Serwery,OU=Komputery,DC=domena,DC=pl' do OU w kt�rym znajduj� si� serwery. Parametr wymagany.

.INPUTS
    String

.OUTPUTS
    Skrypt zwraca obiekt reprezentuj�cy serwer z w�a�ciwo�ciami i typem jak poni�ej:

        TypeName: System.Management.Automation.PSCustomObject

        Name           MemberType   Definition
        ----           ----------   ----------
        Equals         Method       bool Equals(System.Object obj)
        GetHashCode    Method       int GetHashCode()
        GetType        Method       type GetType()
        ToString       Method       string ToString()
        DNS            NoteProperty string[] DNS=System.String[]
        Error          NoteProperty object Error=null
        FreeSpace_(GB) NoteProperty ArrayList FreeSpace_(GB)=System.Collections.ArrayList
        IPAddress      NoteProperty string IPAddress=10.10.10.10
        Memory_(GB)    NoteProperty double Memory_(GB)=8
        OS             NoteProperty string OS=Microsoft Windows Server 2019 Standard     
        S/N            NoteProperty string S/N=-
        ServerName     NoteProperty System.String ServerName=Server1
        ServerType     NoteProperty string ServerType=Virtual Machine
        Services       NoteProperty Object[] Services=System.Object[]

.EXAMPLE
 'OU=Serwery,OU=Komputery,DC=domena,DC=pl' | Get-SRServerInfo.ps1

.EXAMPLE
  Get-SRServerInfo.ps1 -SearchBase 'OU=Serwery,OU=Komputery,DC=domena,DC=pl'

 .NOTES
   Version:        1.1
   Author:         Sebastian Cicho�ski
   Creation Date:  06.2024
   Projecturi:     https://gitlab.com/powershell1990849/get-serverinfo
 #>

 #Requires -RunAsAdministrator

[CmdletBinding()]
param (
    [Parameter(Mandatory, Position=0, ValueFromPipeline)]
    [string] $SearchBase
)

# Pobranie komputer�w z AD
try {
    $Servers = Get-ADComputer -SearchBase $SearchBase -Filter * | Select-Object -ExpandProperty Name -ErrorAction Stop
}
catch {
    Write-Host "Nie mo�na znale�� obiektu: $SearchBase" -ForegroundColor Red
}

$ScriptPath = Split-Path $MyInvocation.MyCommand.Path -Parent

# Pobranie nazw us�ug z pliku '.\services.txt'
try {
    $Services = Get-Content "$ScriptPath\services.txt" -ErrorAction Stop
}
catch {
    Write-Host "Nie mo�na znale�� pliku: [$ScriptPath\services.txt]." -ForegroundColor Red
}


foreach($Server in $Servers) {
    
    try {
        $Output = [PSCustomObject]@{
            'ServerName' = $null
            'IPAddress' = $null
            'DNS' = $null
            'OS' = $null
            'FreeSpace_(GB)' = $null
            'Memory_(GB)' = $null
            'Services' = $null
            'ServerType' = $null
            "S/N" = $null
            'Error' = $null
        }
    
        $Output.ServerName = $Server

        # Przyci�cie nazwy serwera do pierwszych 15 znak�w. 
        # Je�li nazwa serwera jest d�u�sza ni� 15 znak�w r�ni si� od nazwy NetBios
        # wtedy polecenia zwi�zane z sesj� CIM zwracaj� b��d
        If($Server.Length -gt 15) {
            $Server = $Server.Substring(0,15)
        }
        # Sprawdzenie czy komputer jest online, je�li tak tworzona jest sesja CIM
        # kt�ra jest wyko�ystana do pobrania informacji z serwera
        if($null = Test-Connection -ComputerName $Server -Count 1 -ErrorAction Stop) {
            $cimInstanceParams = @{
                CimSession = New-CimSession -ComputerName $Server -ErrorAction SilentlyContinue
            }
            
            # Pobranie inforamcji o wolnej przestrzeni na dyskach, wynik wyra�ony w bajtach
            # jest konwertowany na GB i zaokr�glany �eby nie wy�wietla� miejsc po przecinku
            $discSpace = (Get-CimInstance @cimInstanceParams -ClassName Win32_LogicalDisk).FreeSpace
            $discSpace | ForEach-Object -Begin{ $FreeSpaceList = [System.Collections.ArrayList]@() }  -Process{ if($_){$null = $FreeSpaceList.add([Math]::Round(($_ / 1GB),0))} } 
            $Output.'FreeSpace_(GB)' = $FreeSpaceList
        
            # Pobranie informacji o systemie
            $Output.OS = (Get-CimInstance @cimInstanceParams -ClassName Win32_OperatingSystem).Caption 
        
            # Pobranie informacji o ilo�ci pami�ci, informacje s� podawane oddzielnie dla ka�dego banku pami�ci
            # s� sumowane i zamieniana na GB
            $Output.'Memory_(GB)' = (Get-CimInstance @cimInstanceParams -ClassName Win32_PhysicalMemory | Measure-Object -Property Capacity -Sum).Sum / 1GB

            # Pobranie adresu kart kt�re ten adres maj� przypisany i wybranie adresu IPv4
            $Output.IPAddress = (Get-CimInstance @cimInstanceParams -ClassName Win32_NetworkAdapterConfiguration -Filter "IPEnabled = 'True'" | Select-Object -Property *).IPAddress[0]
        
            # Pobranie adres�w serwer�w DNS
            $Output.DNS = (Get-CimInstance @cimInstanceParams -ClassName Win32_NetworkAdapterConfiguration -Filter "IPEnabled = 'True'" | Select-Object -Property *).DnsServerSearchOrder
        
            # Pobranie modelu serwera, je�li jest to maszyna wirtualna w wyniku b�dzie 'Virtual Machine'
            # je�li jest to serwer fizyczny otrzymamy nazwe modelu np: 'PowerEdge R730xd'
            $Output.'ServerType' = (Get-CimInstance @cimInstanceParams -ClassName Win32_ComputerSystem -ErrorAction Stop ).Model

            # Pobranie us�ug wyszczeg�lnionych w pliku: .\services.txt
            if($Services -ne $null) {
                $Output.Services = (Get-Service -ComputerName $Server -Name $Services -ErrorAction SilentlyContinue).DisplayName
            }

            # Pobranie nueru seryjnego serwera w przypadku gdy jest to serwer fizyczny
            if($Output.'ServerType' -like "Virtual Machine") { 
                $Output.'S/N'= '-' 
            }
            else {
                $Output.'S/N' = (Get-CimInstance @cimInstanceParams -ClassName Win32_bios).SerialNumber
            } 
        }
    }
    catch {
        $Output.Error = $_.Exception.Message
    }
    finally {
        [PSCustomObject]$Output
    }
}